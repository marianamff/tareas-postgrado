%%COVARIANZAS IGUALES
clear all
figure(2)

sp2 = subplot(2,2,2);

hold on
mu1 = [0 0]; sigma = [1 .5;.5 1]; r = mvnrnd(mu1,sigma,200);
%plot(sp2,r(:,1),r(:,2),'+')

x1 = -4:.1:6; x2 = -5:.1:5; [X1,X2] = meshgrid(x1,x2);
F1 = mvnpdf([X1(:) X2(:)],mu1,sigma);
F1 = reshape(F1,length(x2),length(x1));
contour(x1,x2,F1,'-g','LineWidth',0.5);

clear r
mu2 = [1 1]; r = mvnrnd(mu2,sigma,200);
%plot(sp2,r(:,1),r(:,2),'+r')

F2 = mvnpdf([X1(:) X2(:)],mu2,sigma);
F2 = reshape(F2,length(x2),length(x1));
contour(x1,x2,F2,'-m','LineWidth',0.5);

%clasificacion de patrones
%plot(sp2,x1,-x1+1)
legend('C1','C2')
title('Curvas de nivel de Distribuciones')

sp1 = subplot(2,2,1);
mesh(x1,x2,F1);
hold on
mesh(x1,x2,F2);
title('Distribuciones normales a clasificar')

sp3 = subplot(2,2,3);
%tam = length(x1);

clear c1
clear c2
c1 = zeros(1,2);
c2 = zeros(1,2);

s = 100;
n = 12;

r = rand(100,1);

tam = s

for i=1:n
    r = r+rand(100,1);
end

N1 = (r - 6) - 0.5;

r = rand(100,1);

for i=1:n
    r = r+rand(100,1);
end

N2 = (r - 6) - 0.5;

X = mvnrnd([0;0],[1 .5;.5 1],s);
Y = mvnrnd([1;1],[1 .5;.5 1],s);

D1 = mahal([N2 N1],X);
D2 = mahal([N2 N1],Y);

for i=1:tam
    gi = -0.5*D1(i)+0.5*D2(i);    
    if (gi>0)        
        if sum(c1)==0
            c1(1,:)=[N1(i) N2(i)];
        else
            c1=cat(1,c1,[N1(i) N2(i)]);
        end
    else
        if sum(c2)==0
            c2(1,:)=[N1(i) N2(i)];
        else
            c2=cat(1,c2,[N1(i) N2(i)]);
        end
    end    
end
hold on

plot(c1(:,1),c1(:,2),'+b');
plot(c2(:,1),c2(:,2),'+r');

legend(strcat('CC=',int2str(length(c1))),strcat('CI=',int2str(length(c2))))
title('Generación y clasificación de muestras para la clase C1')


hold on 

sp4 = subplot(2,2,4);

clear cc1
clear cc2
clear D1

cc1 = zeros(1,2);
cc2 = zeros(1,2);

s = 100;
n = 12;

r = rand(100,1);

tam = s;

for i=1:n
    r = r+rand(100,1);
end

N3 = (r - 6) + 0.5;

r = rand(100,1);

for i=1:n
    r = r+rand(100,1);
end

N4 = (r - 6) + 0.5;

X = mvnrnd([0;0],[1 .5;.5 1],s);
Y = mvnrnd([1;1],[1 .5;.5 1],s);

D1 = mahal([N3 N4],X);
D2 = mahal([N3 N4],Y);

for i=1:tam
    gi = -0.5*D1(i)+0.5*D2(i);  
    if (gi>0)        
        if sum(cc1)==0
            cc1(1,:)=[N3(i) N4(i)];
        else
            cc1=cat(1,cc1,[N3(i) N4(i)]);
        end
    else
        if sum(cc2)==0
            cc2(1,:)=[N3(i) N4(i)];
        else
            cc2=cat(1,cc2,[N3(i) N4(i)]);
        end
    end    
end

hold on
plot(cc1(:,1),cc1(:,2),'+r');
plot(cc2(:,1),cc2(:,2),'+b');

legend(strcat('CC=',int2str(length(cc2))),strcat('CI=',int2str(length(cc1))))
title('Generación y clasificación de muestras para la clase C2')

%sp5 = subplot(3,2,5);

% scatter(X(:,1),X(:,2))
% hold on
% contour(N3,N4,D1)
% hb = colorbar;
% ylabel(hb,'Distancia de Mahalanobis')
% legend('X','Y','Location','NW')