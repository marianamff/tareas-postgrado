%%COVARIANZAS IGUALES
clear all
figure(2)

sp2 = subplot(3,2,2);

hold on
mu1 = [0 0]; sigma = [1 0; 0 1]; r = mvnrnd(mu1,sigma,200);
plot(sp2,r(:,1),r(:,2),'+')

x1 = -4:.1:6; x2 = -5:.1:5; [X1,X2] = meshgrid(x1,x2);
F1 = mvnpdf([X1(:) X2(:)],mu1,sigma);
F1 = reshape(F1,length(x2),length(x1));
%contour(x1,x2,F1,'-g','LineWidth',0.5);

clear r
mu2 = [1 1]; r = mvnrnd(mu2,sigma,200);
plot(sp2,r(:,1),r(:,2),'+r')

F2 = mvnpdf([X1(:) X2(:)],mu2,sigma);
F2 = reshape(F2,length(x2),length(x1));
%contour(x1,x2,F2,'-m','LineWidth',0.5);

%clasificacion de patrones
plot(sp2,x1,-x1+1)
%legend('C1','C1','C2','C2','G(X)')
legend('C1','C2','G(X)')
title('Distribuciones y función discriminante')

sp1 = subplot(3,2,1);
mesh(x1,x2,F1);
hold on
mesh(x1,x2,F2);
title('Distribuciones normales a clasificar')

sp3 = subplot(3,2,3);
%tam = length(x1);

clear c1
clear c2
c1 = zeros(1,2);
c2 = zeros(1,2);

%calculo de puntos aleatorios de una normal

s = 100;
n = 12;

r = rand(100,1);

tam = s

for i=1:n
    r = r+rand(100,1);
end

N1 = (r - 6) - 0.5;

r = rand(100,1);

for i=1:n
    r = r+rand(100,1);
end

N2 = (r - 6) - 0.5;

%clasificador

for i=1:tam
    gi = -N1(i)-N2(i)+1;    
    if (gi>0)        
        if sum(c1)==0
            c1(1,:)=[N1(i) N2(i)];
        else
            c1=cat(1,c1,[N1(i) N2(i)]);
        end
    else
        if sum(c2)==0
            c2(1,:)=[N1(i) N2(i)];
        else
            c2=cat(1,c2,[N1(i) N2(i)]);
        end
    end    
end
hold on
plot(sp3,x1,-x1+1,'c')
plot(c1(:,1),c1(:,2),'+r');
plot(c2(:,1),c2(:,2),'+b');

legend('G(X)',strcat('CC=',int2str(length(c1))),strcat('CI=',int2str(length(c2))))
title('Generación y clasificación de muestras para la clase C1')

sp4 = subplot(3,2,4);

clear cc1
clear cc2
cc1 = zeros(1,2);
cc2 = zeros(1,2);

s = 100;
n = 12;

r = rand(100,1);

tam = s;

for i=1:n
    r = r+rand(100,1);
end

N3 = (r - 6) + 0.5;

r = rand(100,1);

for i=1:n
    r = r+rand(100,1);
end

N4 = (r - 6) + 0.5;

for i=1:tam
    gi = -N3(i)-N4(i)+1;    
    if (gi<=0)        
        if sum(cc1)==0
            cc1(1,:)=[N3(i) N4(i)];
        else
            cc1=cat(1,cc1,[N3(i) N4(i)]);
        end
    else
        if sum(cc2)==0
            cc2(1,:)=[N3(i) N4(i)];
        else
            cc2=cat(1,cc2,[N3(i) N4(i)]);
        end
    end    
end

hold on
plot(sp4,x1,-x1+1,'c')
y = zeros(100,1);
plot(cc1(:,1),cc1(:,2),'+r');
plot(cc2(:,1),cc2(:,2),'+b');

legend('G(X)',strcat('CC=',int2str(length(cc1))),strcat('CI=',int2str(length(cc2))))
title('Generación y clasificación de muestras para la clase C2')

sp5 = subplot(3,2,5);
B = 0:.1:1;
C = zeros(length(B),1);
E = zeros(length(B),1);
for i=1:length(B)
    C(i)= Chernoff([0 0], [1 0;0 1], [1 1], [1 0;0 1], B(i));
    E(i)= exp(-C(i));
end

c = Chernoff([0 0], [1 0;0 1], [1 1], [1 0;0 1], 0.5);

plot(sp5,B,E);
legend('Cota de Chernoff')
title(strcat('Cota de Chernoff:  ',num2str(c)))

%contour(x1,x2,F1-F2,[-0.01 0.00 0.01 0.05 0.1 0.15 0.17], 'g','LineWidth',2)
%contour(x1,x2,log(F1)-log(F2),[-15 0.00 15], ':r','LineWidth',2)