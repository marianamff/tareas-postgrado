function varargout = hopfieldGra(varargin)
% HOPFIELDGRA MATLAB code for hopfieldGra.fig
%      HOPFIELDGRA, by itself, creates a new HOPFIELDGRA or raises the existing
%      singleton*.
%
%      H = HOPFIELDGRA returns the handle to a new HOPFIELDGRA or the handle to
%      the existing singleton*.
%
%      HOPFIELDGRA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HOPFIELDGRA.M with the given input arguments.
%
%      HOPFIELDGRA('Property','Value',...) creates a new HOPFIELDGRA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before hopfieldGra_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to hopfieldGra_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help hopfieldGra

% Last Modified by GUIDE v2.5 04-Jun-2015 19:37:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @hopfieldGra_OpeningFcn, ...
                   'gui_OutputFcn',  @hopfieldGra_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before hopfieldGra is made visible.
function hopfieldGra_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to hopfieldGra (see VARARGIN)

% Choose default command line output for hopfieldGra
handles.output = hObject;

% This sets up the initial plot - only do when we are invisible
% so window can get raised using hopfieldGra.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end
%scrsz = get(groot,'ScreenSize');
%set(handles.figure1,'Position',[scrsz(3)*.2 scrsz(4)*.2 scrsz(3)*.6 scrsz(4)*.6])

handles.N = 30;
NP = 3;

axis([-1 handles.N+2 -1 handles.N+2 ])
cla;

sp2 = subplot(1,2,1);
sp3 = subplot(1,2,2);

P=zeros(NP,handles.N*handles.N);
P(1,:)=loadImg('images/a.jpg');
P(2,:)=loadImg('images/e.jpg');
P(3,:)=loadImg('images/i.jpg');


I = eye(handles.N*handles.N);
handles.W = zeros(handles.N*handles.N);

for i=1:NP
    wk = double(P(i,:))'*double(P(i,:)) - I;
    handles.W = handles.W + wk;  
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes hopfieldGra wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = hopfieldGra_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%axes(handles.axes1);
axis([-1 handles.N+2 -1 handles.N+2 ])
cla;

ruido = str2double(get(handles.edit2, 'String'));

popup_sel_index = get(handles.popupmenu1, 'Value');
switch popup_sel_index
    case 1
        PP = loadImg('images/a.jpg');
    case 2
        PP = loadImg('images/e.jpg');
    case 3
        PP = loadImg('images/i.jpg');
    
end

PP = aplicarRuido(PP,ruido);

%scrsz = get(groot,'ScreenSize');
%figure('Position',[scrsz(3)*.1 scrsz(4)*.1 scrsz(3)*.8 scrsz(4)*.8])

sp1 = subplot(1,2,1);
plotMatris(PP,handles.N,sp1)
title('Patron a clasificar')

PW = PP'*handles.W;

n = length(PW);
SP = [1,length(PW)];
SPOLD = [1,length(PW)];
cont = 0;
ig = 0;
while (cont<200 && ig<10)
    SPOLD=SP;
    res = isequaln(SPOLD,SP);
    if (res && cont~=0)
        ig=ig+1;
    end
    
    for i=1:n
        if (PW(i)>=0)
            SP(i)=1;
        else
            SP(i)=-1;   
        end

    end
    cont = cont +1;
end 


PS_ = reshape(SP,handles.N,handles.N)';

sp2 = subplot(1,2,2);
title('Clasificación asignada al patron')
plotMatris(SP,handles.N,sp2)


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'a', 'e', 'i', 'o', 'u'});



% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
holfieldIni();



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
