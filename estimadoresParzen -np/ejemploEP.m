%%n�cleo hiperc�bico con ancho ?= 5 
clear all
figure(2)

PX = zeros(1,6);
PC1 = [1 2 5 6 7 9];
PC2 = [11 14 15 16 19 20];

p= 5;

N=6;
P=1/((N)*(2*p));

sp1 = subplot(2,2,1);
plot(sp1,PC1,PX,'o',PC2,PX,'o')
axis([-10 30 -0.001 0.2])


X=(-10:0.1:30);
n = length(X);
Yc1=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc1(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Yc2=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC2(j) &&  PC2(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc2(i) = c*P;
end

%sp2 = subplot(3,2,2);
hold on;
plot(sp1,X,Yc1,X,Yc2)
legend('C1','C2','P(X/c1)','P(X/c2)')
title('N�cleo hiperc�bico con ancho p= 5 ')


%n�cleo gaussiano con ancho ?= 3 


p= 3;

N=6;
P=1/((N)*p*sqrt(2*pi));

sp2 = subplot(2,2,2);
plot(sp2,PC1,PX,'o',PC2,PX,'o')
axis([-10 30 -0.001 0.2])


X=(-10:0.1:30);
n = length(X);
Ygc1=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
%         if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
%             c=c+1;
%         end
        c=c+exp(-1/2*((X(i)-PC1(j))/p)^2);
    end
    Ygc1(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Ygc2=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        c=c+exp(-1/2*((X(i)-PC2(j))/p)^2);
    end
    Ygc2(i) = c*P;
end

hold on;
plot(sp2,X,Ygc1,X,Ygc2)
legend('C1','C2','P(X/c1)','P(X/c2)')
title('N�cleo gaussiano con ancho p= 3')

sp3 = subplot(2,2,[3 4]);
plot(sp3,X,Ygc1,X,Ygc2)
hold on;
plot(sp3,X,Yc1,X,Yc2)
plot(sp3,PC1,PX,'o',PC2,PX,'o')
axis([-10 30 -0.001 0.2])
legend('Clase 1','Clase 2')
title('Comparaci�n entre estimaciones')

