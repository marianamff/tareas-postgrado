%%n�cleo hiperc�bico con ancho ?= 5 
clear all
figure(2)

PX = zeros(1,6);
PC1 = [1 2 5 6 7 9];
PC2 = [11 14 15 16 19 20];

p= 1;

N=6;
P=1/((N)*(2*p));

sp1 = subplot(1,2,1);
plot(sp1,PC1,PX,'o',PC2,PX,'o')
axis([-10 30 -0.001 0.25])


X=(-10:0.1:30);
n = length(X);
Yc1=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc1(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Yc2=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC2(j) &&  PC2(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc2(i) = c*P;
end

%sp2 = subplot(3,2,2);
hold on;
plot(sp1,X,Yc1,X,Yc2)
title('N�cleo hiperc�bico con ancho p= 5 ')


%p=1

p= 5;

P=1/((N)*(2*p));

Yc1p1=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc1p1(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Yc2p1=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC2(j) &&  PC2(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc2p1(i) = c*P;
end

%sp2 = subplot(3,2,2);
hold on;
plot(sp1,X,Yc1p1,'r',X,Yc2p1,'c')

%p=1

p= 7;

P=1/((N)*(2*p));

Yc1p7=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc1p7(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Yc2p7=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        if (X(i)-p<= PC2(j) &&  PC2(j)<=X(i)+p )
            c=c+1;
        end
    end
    Yc2p7(i) = c*P;
end

%sp2 = subplot(3,2,2);
hold on;
plot(sp1,X,Yc1p7,'y',X,Yc2p7,'m')
legend('C1','C2','C1 p=1','C2 p=1','C1 p=5','C2 p=5','C1 p=7','C2 p=7')
title('N�cleo hiperc�bico ')

%n�cleo gaussiano con ancho ?= 3 


p= 3;

N=6;
P=1/((N)*p*sqrt(2*pi));

sp2 = subplot(1,2,2);
plot(sp2,PC1,PX,'o',PC2,PX,'o')
axis([-10 30 -0.001 0.25])


X=(-10:0.1:30);
n = length(X);
Ygc1=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
%         if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
%             c=c+1;
%         end
        c=c+exp(-1/2*((X(i)-PC1(j))/p)^2);
    end
    Ygc1(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Ygc2=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        c=c+exp(-1/2*((X(i)-PC2(j))/p)^2);
    end
    Ygc2(i) = c*P;
end

hold on;
plot(sp2,X,Ygc1,X,Ygc2)



p= 5;

P=1/((N)*p*sqrt(2*pi));
Ygc1p5=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
%         if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
%             c=c+1;
%         end
        c=c+exp(-1/2*((X(i)-PC1(j))/p)^2);
    end
    Ygc1p5(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Ygc2p5=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        c=c+exp(-1/2*((X(i)-PC2(j))/p)^2);
    end
    Ygc2p5(i) = c*P;
end

hold on;
plot(sp2,X,Ygc1p5,'r',X,Ygc2p5,'c')

p= 7;

P=1/((N)*p*sqrt(2*pi));
Ygc1p7=zeros(1,n);
for i=1:n
    m = length(PC1);
    c = 0;
    for j=1:m
%         if (X(i)-p<= PC1(j) &&  PC1(j)<=X(i)+p )
%             c=c+1;
%         end
        c=c+exp(-1/2*((X(i)-PC1(j))/p)^2);
    end
    Ygc1p7(i) = c*P;
end

X=(-10:0.1:30);
n = length(X);
Ygc2p7=zeros(1,n);
for i=1:n
    m = length(PC2);
    c = 0;
    for j=1:m
        c=c+exp(-1/2*((X(i)-PC2(j))/p)^2);
    end
    Ygc2p7(i) = c*P;
end

hold on;
plot(sp2,X,Ygc1p7,'y',X,Ygc2p7,'m')

legend('C1','C2','C1 p=3','C2 p=3','C1 p=5','C2 p=5','C1 p=7','C2 p=7')
title('N�cleo gaussiano')


