clear all
%figure(2)
scrsz = get(groot,'ScreenSize');
figure('Position',[(scrsz(3)-scrsz(3)*0.8)/2 (scrsz(4)-scrsz(4)*0.8)/2 scrsz(3)*0.8 scrsz(4)*0.8])


%sp1 = subplot(1,2,1);
axis([-5 23 -5 23])
daspect([1,1,1])

t=5;
O=3/5;


%X=[0 2 1 1 5 3 6 6 6 7 3];
%Y=[0 8 2 1 3 9 2 3 4 3 6];
m = [randi([1,4],1,2);randi([8,10],1,2); randi([15,20],1,2)];
X= [normrnd(m(1,1),1,[1 40]) normrnd(m(2,1),1,[1 40]) normrnd(m(3,1),1,[1 40])];
Y= [normrnd(m(1,2),1,[1 40]) normrnd(m(2,2),1,[1 40]) normrnd(m(3,2),1,[1 40])];

X = X-X(1);
Y = Y-Y(1);
XX = X;
YY = Y;
orD = randperm(length(X));

for j=1:length(X)
   X(j) = XX(orD(j));    
   Y(j) = YY(orD(j));   
end


n = length(X);
cX = cell(n,2);

%plot patrones
for j=1:length(X)
   cX{j,1} = line('XData',X(j), 'YData',Y(j), 'Color',[0.8 0.8 0.8], 'Marker','o', 'MarkerSize',6);    
   cX{j,2} = text(X(j), Y(j), sprintf('%d',j), 'Color',[0.6 0.6 0.6], 'FontSize',8, 'HorizontalAlignment','left', 'VerticalAlignment','top');
end

pause(1)

S=zeros(n,1);
S(1)=1;
Z=zeros(n,2);
iz = 2; %numero de clases
cL = cell(1,2); %lista de graficos

TAs = [(1:n); zeros(2,n)];
%0 sin procesar
%1 asignado
%-1 indeterminado

Z(1,:)=[X(1) Y(1)];
TAs(2,1)=1;
TAs(3,1)=1;

%<--plot primera clase
cL{1,2} = line('XData',Z(1,1), 'YData',Z(1,2), 'Color','g', 'Marker','x');
        
r = t*O;
xc = Z(1,1);
yc = Z(1,2);

theta = linspace(0,2*pi);
x = r*cos(theta) + xc;
y = r*sin(theta) + yc;

cL{1,1} = line('XData',x, 'YData',y, 'Color','g');
set(cX{1,1}, 'XData',X(1), 'YData',Y(1), 'Color','g')  

%pause(1)
%plot primera clase/-->

iI = 2;
ciI = 0;
TAs_ = zeros(size(TAs));

while (isequal(TAs_,TAs)==false && ciI<10)
    TAs_ =TAs;
    
   
    for i=iI:n
        
        title(['Iteraci�n:' num2str(ciI) '  Asignando clase a patron: ' num2str(i)]);
         
        set(cX{i,2},'Color',[1 0.3 0],'FontSize',10)  
        %pause(1)
         
        p = [X(i) Y(i)];
        d_ = -1;
        d = -1;
        c_ = 0;

        %calcular las distancias
        for j=1:n
            if (j<iz) 
                d = sqrt(sum((p-Z(j,:)).^2));
                if (j==1)
                    d_ = d;
                    c_ = j;
                elseif (d_>d)
                    d_ = d;
                    c_ = j;
                end
            else
                break;
            end          
        end

        %asignar clase    
        if (d_<t && d_<t*O)
            
            TAs(2,i)=1;
            TAs(3,i)=c_;
            
            
            vas = c_;            
            c = 'r';
            if (vas==1)
                c = 'g';
            elseif (vas==2)
                c = 'c';
            elseif (vas==3)
                c = 'm';
            elseif (vas==4)
                c = 'y';
            end
            
            set(cX{i,1}, 'XData',X(i), 'YData',Y(i), 'Color',c)   
            drawnow
            %pause(1)
            
        elseif (d_<t)
            TAs(2,i)=-1;
            TAs(3,i)=0; 
            
            set(cX{i,1}, 'XData',X(i), 'YData',Y(i), 'Color','r')   
            drawnow
            %pause(1)
            
        else
            Z(iz,:)=p;
            TAs(2,i)=1;
            TAs(3,i)=iz;  
            iz = iz +1;
            
            vas = iz-1;         
            c = 'r';
            if (vas==1)
                c = 'g';
            elseif (vas==2)
                c = 'c';
            elseif (vas==3)
                c = 'm';
            elseif (vas==4)
                c = 'y';
            end
            
            cL{vas,2} = line('XData',Z(vas,1), 'YData',Z(vas,2), 'Color',c, 'Marker','x');
        
            r = t*O;
            xc = Z(vas,1);
            yc = Z(vas,2);

            theta = linspace(0,2*pi);
            x = r*cos(theta) + xc;
            y = r*sin(theta) + yc;

            cL{j,1} = line('XData',x, 'YData',y, 'Color',c);
            
            set(cX{i,1}, 'XData',X(i), 'YData',Y(i), 'Color',c)   

            %pause(1)
        
        end
        
       
        set(cX{i,2},'Color',[0.2 0.2 0.2],'FontSize',8)  
        pause(0.1)
    end
    
    title(['Iteraci�n:' num2str(ciI) ' Recalculando centros']);

    %recalculo los centros
    for j=1:iz-1
        cont=0;
        pP = [0 0];
        for m=1:n  
            vas = TAs(3,m);
            if (vas==j) 
                pN = [X(m) Y(m)];
                pP = pP + pN; 
                cont = cont+1;
            end          
        end
        pP = pP./cont; %calculo el prom 

        Z(j,:)=pP;%[X(c_) Y(c_)];     
    end

    iI = 1;
    ciI = ciI+1;
       
    %set los centros   
    for j=1:iz-1        
        c = 'b';
        if (j==1)
            c = 'g';
        elseif (j==2)
            c = 'c';
        elseif (j==3)
            c = 'm';
        elseif (j==5)
            c = 'y';
        end
        
        t_cL = size(cL,1);
        
        if (iz-1>t_cL)
            cL{j,2} = line('XData',Z(j,1), 'YData',Z(j,2), 'Color',c, 'Marker','x');
        else
            set(cL{j,2}, 'XData',Z(j,1), 'YData',Z(j,2))   
            drawnow
        end
        
        
        r = t*O;
        xc = Z(j,1);
        yc = Z(j,2);

        theta = linspace(0,2*pi);
        x = r*cos(theta) + xc;
        y = r*sin(theta) + yc;
        
        
        if (iz-1>t_cL)
            cL{j,1} = line('XData',x, 'YData',y, 'Color',c);
        else
            set(cL{j,1}, 'XData',x, 'YData',y)   
            drawnow
        end
        
        pause(1)
    end
    
   
end


title(['Total de Iteraciones:' num2str(ciI) ' N�mero de clases encontradas: ' num2str(iz-1)]);