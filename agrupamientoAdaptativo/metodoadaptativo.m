t=3;
O=2/3;
X=[0 2 1 1 5 3 6 6 6 7 3];
Y=[0 8 2 1 3 9 2 3 4 3 6];

n = length(X);
S=zeros(n,1);
S(1)=1;
Z=zeros(n,2);
iz = 2;

TAs = [(1:11); zeros(2,n)];
%0 sin procesar
%1 asignado
%-1 indeterminado

TAs(2,1)=1;
TAs(3,1)=1;

iI = 2;
ciI = 0;
TAs_ = zeros(size(TAs));

while (isequal(TAs_,TAs)==false && ciI<10)
TAs_ =TAs;
for i=iI:n
    p = [X(i) Y(i)];
    d_ = -1;
    d = -1;
    c_ = 0;
    
    %calcular las distancias
    for j=1:n
        if (j<iz) 
            d = sqrt(sum((p-Z(j,:)).^2));
            if (j==1)
                d_ = d;
                c_ = j;
            elseif (d_>d)
                d_ = d;
                c_ = j;
            end
        else
            break;
        end          
    end
    
    %asignar clase    
    if (d_<t && d_<t*O)
        TAs(2,i)=1;
        TAs(3,i)=c_;        
    elseif (d_<t)
        TAs(2,i)=-1;
        TAs(3,i)=0;    
    else
        Z(iz,:)=p;
        TAs(2,i)=1;
        TAs(3,i)=iz;  
        iz = iz +1;
    end
end

%recalculo los centros
for j=1:iz-1
    cont=0;
    pP = [0 0];
    for m=1:n  
        vas = TAs(3,m);
        if (vas==j) 
            pN = [X(m) Y(m)];
            pP = pP + pN; 
            cont = cont+1;
        end          
    end
    pP = pP./cont; %calculo el prom 
   
%     d_ = 0;
%     c_ = 0;
%     
%     for m=1:n
%         vas = TAs(3,m);
%         if (vas==j) 
%             d = sqrt(sum(([X(m) Y(m)]-pP).^2));
%             if (d_==0)
%                 d_ = d;
%                 c_ = m;
%             elseif (d_>=d)
%                 d_ = d;
%                 c_ = m;
%             end
%         end          
%     end
    
    Z(j,:)=pP;%[X(c_) Y(c_)];     
end

iI = 1;
ciI = ciI+1;
end

figure(2)

%plot
    for j=1:length(X)
        vas = TAs(3,j);
        c = 'ro';
        if (vas==1)
            c = 'bo';
        elseif (vas==2)
            c = 'co';
        elseif (vas==3)
            c = 'mo';
        end
        hold on    
        plot(X(j),Y(j),c);        
    end
    
    for j=1:iz-1
        
        c = 'bx';
        if (j==1)
            c = 'bx';
        elseif (j==2)
            c = 'cx';
        elseif (j==3)
            c = 'mx';
        end
        hold on    
        plot(Z(j,1),Z(j,2),c);        
        
        r = t*O;
        xc = Z(j,1);
        yc = Z(j,2);

        theta = linspace(0,2*pi);
        x = r*cos(theta) + xc;
        y = r*sin(theta) + yc;
        hold on   
        plot(x,y,c)
    end
axis([-2 12 -2 12])



