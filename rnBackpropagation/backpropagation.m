%%Inicializando
clear all
h = figure(2);

EP = 1; %numero de iteraciones 
EPm = 1000; %numero maximo de iteraciones 
ERRc = 0.00001; % Cota error 
Um = 0.1;
CA = 0.5; %CONSTANTE DE APRENDIZAJE

NNCO = 6; %(NUMERO DE NEURONAS DE la capa oculta)
NC = 3; %Numero de capas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%VAR
% NP = 8; %Numero de patrones a utilizar en el entrenamiento
% NNS = 1; %Tama�o de las Salidas (NUMERO DE NEURONAS DE SALIDAS)
% N = 307200; %Tama�o de los patrones (NUMERO DE NEURONAS DE ENTRADAS)
% NP = 4; %Numero de patrones a utilizar en el entrenamiento
% NNS = 1; %Tama�o de las Salidas (NUMERO DE NEURONAS DE SALIDAS)
% N = 75000; %Tama�o de los patrones (NUMERO DE NEURONAS DE ENTRADAS)
% NP = 4; %Numero de patrones a utilizar en el entrenamiento
% NNS = 1; %Tama�o de las Salidas (NUMERO DE NEURONAS DE SALIDAS)
% N = 307200; %Tama�o de los patrones (NUMERO DE NEURONAS DE ENTRADAS)

% NP = 4; %Numero de patrones a utilizar en el entrenamiento
% NNS = 1; %Tama�o de las Salidas (NUMERO DE NEURONAS DE SALIDAS)
% N = 2; %Tama�o de los patrones (NUMERO DE NEURONAS DE ENTRADAS)

NP = 4; %Numero de patrones a utilizar en el entrenamiento
NNS = 1; %Tama�o de las Salidas (NUMERO DE NEURONAS DE SALIDAS)
N = 100; %Tama�o de los patrones (NUMERO DE NEURONAS DE ENTRADAS)

% NP = 5; %Numero de patrones a utilizar en el entrenamiento
% NNS = 1; %Tama�o de las Salidas (NUMERO DE NEURONAS DE SALIDAS)
% N = 90000; %Tama�o de los patrones (NUMERO DE NEURONAS DE ENTRADAS)

P=zeros(NP,N);

% P(1,:)=[1 0];
% P(2,:)=[0 0];
% P(3,:)=[0 1];
% P(4,:)=[1 1];


sP = [3 4 1];
P(1,:)=loadImgBinP('images/1.jpg',sP);
title('P1 salida asignada: 0.2')
sP = [3 4 2];
P(2,:)=loadImgBinP('images/2.jpg',sP);
title('P2 salida asignada: 0.4')

sP = [3 4 3];
P(3,:)=loadImgBinP('images/3.jpg',sP);
title('P3 salida asignada: 0.6')

sP = [3 4 4];
P(4,:)=loadImgBinP('images/4.jpg',sP);
title('P4 salida asignada: 0.8')

Y=[ 0.2;
    0.4;
    0.6;
    0.8;]; 

% sP = [3 4 1];
% P(1,:)=loadImgBinP('images/orq/catt.jpg',sP);
% title('P1 salida asignada: 0.2')
% sP = [3 4 2];
% P(2,:)=loadImgBinP('images/orq/cymbi.jpg',sP);
% title('P2 salida asignada: 0.4')
% 
% sP = [3 4 3];
% P(3,:)=loadImgBinP('images/orq/lady.jpg',sP);
% title('P3 salida asignada: 0.6')
% 
% sP = [3 4 4];
% P(4,:)=loadImgBinP('images/orq/phala.jpg',sP);
% title('P4 salida asignada: 0.8')
% 
% P(5,:)=loadImgBin('images/orq/lady-2.jpg');
% 
% 
% Y=[ 0.2;
%     0.4;
%     0.6;
%     0.8;
%     0.6;]; 

% P(1,:)=loadImgBin('images/autos/cam-i.jpg');
% P(2,:)=loadImgBin('images/autos/aut-i.jpg');
% P(3,:)=loadImgBin('images/autos/moto-i.jpg');
% P(4,:)=loadImgBin('images/autos/moto-iii.jpg');
% P(5,:)=loadImgBin('images/autos/cam-ii.jpg');
% P(6,:)=loadImgBin('images/autos/aut-ii.jpg');
% P(7,:)=loadImgBin('images/autos/moto-ii.jpg');
% P(8,:)=loadImgBin('images/autos/cam-iv.jpg');

% P(1,:)=loadImgBin('images/rostros/cuadrado-2.jpg');
% P(2,:)=loadImgBin('images/rostros/diamante-2.jpg');
% P(3,:)=loadImgBin('images/rostros/cuadrado-1.jpg');
% P(4,:)=loadImgBin('images/rostros/diamante-1.jpg');

% P(1,:)=loadImgBin('images/autos/auto-1.jpg');
% P(2,:)=loadImgBin('images/autos/moto-2.jpg');
% P(3,:)=loadImgBin('images/autos/auto-2.jpg');
% P(4,:)=loadImgBin('images/autos/moto-1.jpg');
% Y=[ 0.2; %auto
%     0.8; %moto
%     0.2;
%     0.8;
%     ]; 

 
% Y=[ 0.2;
%     0.9 ;
%     0.2;
%     0.9;]; %zeros(NP,NNS); %%Salidas deseadas



% Y=[ 0.2; %auto
%     0.2; 
%     0.8; %moto
%     0.8;
%     0.2;
%     0.2;
%     0.8;
%     0.2;]; 
% 
% Y=[ 0.2; %cuadrado
%     0.8; %diamante
%     0.2; 
%     0.8;]; 


NpC = [N NNCO NNS]; %Numero de neuronas  por capas

%Inicializar matriz de pesos para cada capa
W = cell(1,NC-1);  


for i=1:NC-1
     W{1,i} = (0.5-rand(NpC(1,i),NpC(1,i+1)));
end
W{end} = (0.5-rand(NpC(end-1),NpC(end)));

%Wall = cell(1,1);  Wall{1,1}=W; contWall=2; Wall{1,2}=EP; Wall{1,3}=100;

%Inicializar matriz de bias para cada capa
Bs = cell(1,NC-1);  
for i=1:NC-1
     Bs{1,i} = (0.5-rand(1,NpC(1,i+1)));
end
Bs{end} = (0.5-rand(1,NpC(end)));

ERR = 100;
ErrP = zeros(1,NP);
ERRs = zeros(1,EPm);

sP = subplot(3,4,[5 8]);


while ERR>ERRc && EP<EPm
    for p=1:NP
        %Propagacion DE LA CAPA ENTRADA A LA CAPA OCULTA
        numc = 1;
        Nnc = NpC(numc+1); %posicion donde esta el tam de la capa oculta en este caso solo 1
        Wc = W{1,numc};
        Bsc= Bs{1,numc};
        A = P(p,:)*Wc + Bsc;%;
        I = logsig(A);

        %Propagacion DE LA CAPA OCULTA A LA CAPA SALIDA

        numc = numc + 1;
        Nnc = NpC(numc+1); %posicion donde esta el tam de la capa oculta en este caso solo 1
        Wc = W{1,numc};
        Bsc= Bs{1,numc};
        B = I*Wc +Bsc;%;  
        O = logsig(B);
        
        %SE CALCULA EL ERROR
        y = Y(p,:);
        e = 0.5*sum((y-O).^2);
        
        es = (y-O).*O.*(1-O);
        
        ErrP(p)=e;
                
        %Actualizaci�n de pesos de la capa de salida
        WC = W{1,numc};
        WCoc = W{1,numc}; %Los t�rminos de error de las unidades ocultas, se calculan antes de que hayan sido modificado los pesos de conexiones con las unidades de la capa de salida.
        BSC = Bs{1,numc};
        for k=1:Nnc %k: ite de neuronas de la capa de salida
             w = WC(:,k); %w: pesos de la neurona   
             w = w + CA*es(k)*I'; %suma de los cuadrados de los errores en todas las unidades de salida
             BSC(k) = BSC(k)+ CA*es(k); %suma de los cuadrados de los errores en todas las unidades de salida
             WC(:,k) = w;
        end
        W{1,numc}=WC;
        Bs{1,numc} = BSC;
       
        %Actualizaci�n de pesos de la capa oculta
        Nnco = NpC(numc);
        WC = W{1,numc-1}; %pesos de la capa oculta
        WS = WCoc; %W{1,numc}; %pesos sin actualizar de la capa de salida
        BSC = Bs{1,numc-1}; %bias de la capa oculta
        Pp = P(p,:);
        
        Eh = I.*(1-I); %
        
        for j=1:Nnco  %j: ite de neuronas de la capa oculta
            Ekw = sum(WS(j,:).*es); %t�rminos de errores de la capa de salida             
            w = WC(:,j); %pesos j de la capa oculta  
            w = w + CA*Eh(j)*Ekw*Pp'; 
            BSC(j) = BSC(j)+ CA*Eh(j)*Ekw;%
            WC(:,j) = w;
        end
        W{1,numc-1}=WC;
        Bs{1,numc-1} = BSC;
        
        %Wall{contWall,1}=W; contWall=contWall+1; Wall{contWall,2}=EP; 
        if ~ishandle(h), break; end     
    
    end        
    
    if ~ishandle(h), break; end     
    
    ERR=sum(ErrP)*(1/NP);
    %Wall{contWall,3}=ERR;
    
    ERRs(EP)=ERR;
    
    hold on
    plot(sP,EP,ERR,'r.')
    title(['Error: ' num2str(ERR)])
    drawnow
    hold off
    
    EP = EP + 1;
    ErrP = zeros(1,NP);        
    
    
end


% sP = [3 4 9];
% P(1,:)=loadImgBinP('images/orq/catt-2.jpg',sP);
% title(['Clasificado: ' num2str(compred(W,Bs,P(1,:)))])
% sP = [3 4 10];
% P(2,:)=loadImgBinP('images/orq/cymbi-3.jpg',sP);
% title(['Clasificado: ' num2str(compred(W,Bs,P(2,:)))])
% sP = [3 4 11];
% P(3,:)=loadImgBinP('images/orq/lady-2.jpg',sP);
% title(['Clasificado: ' num2str(compred(W,Bs,P(3,:)))])
% sP = [3 4 12];
% P(4,:)=loadImgBinP('images/orq/phala.jpg',sP);
% title(['Clasificado: ' num2str(compred(W,Bs,P(4,:)))])

sP = [3 4 9];
P(1,:)=loadImgBinP('images/1.jpg',sP);
title(['Clasificado: ' num2str(compred(W,Bs,P(1,:)))])
sP = [3 4 10];
P(2,:)=loadImgBinP('images/2r.jpg',sP);
title(['Clasificado: ' num2str(compred(W,Bs,P(2,:)))])
sP = [3 4 11];
P(3,:)=loadImgBinP('images/3.jpg',sP);
title(['Clasificado: ' num2str(compred(W,Bs,P(3,:)))])
sP = [3 4 12];
P(4,:)=loadImgBinP('images/4r.jpg',sP);
title(['Clasificado: ' num2str(compred(W,Bs,P(4,:)))])



