function PIm = loadImgBinP(p,sP)

PI = imread(p);
PIB = im2bw(PI);

PIBu = PIB(:);
num = numel(PIBu);
PIBd = zeros(num,1);

for i=1:num
    val = double(PIBu(i));
    if (val==0)
        PIBd(i)= 1;
    else
        PIBd(i)= 0;
    end
    
end

%PF = reshape(PIBd,sqrt(num),sqrt(num));
PIm = PIBd;
hold on
subplot(sP(1),sP(2),sP(3));
subimage(PIB);
drawnow
hold off

